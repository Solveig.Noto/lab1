package rockPaperScissors;
//
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while (true) {
            System.out.println("Let's play round " + roundCounter);

            String humanchoice = userChoice();

            String computerchoice = computerChoice();

            if (findWinner(humanchoice, computerchoice)) {
                System.out.println("Human chose " + humanchoice + ", computer chose " + computerchoice + ". Human win!");
                humanScore++; //++ betyr pluss 1.
            } else if (findWinner(computerchoice, humanchoice)) {
                System.out.println("Human chose " + humanchoice + ", computer chose "  + computerchoice + ". Computer win!");
                computerScore++;
            } else {
                System.out.println("Human chose " + humanchoice + ", computer chose " + computerchoice + ". It's a tie!");
            }

            System.out.println("Score: human " + humanScore + ", computer " + computerScore);


            String option = readInput("Do you wish to continue playing? (y/n)?");
            if (("y").equals(option)) {
                roundCounter++;
                continue;
            } else if (!("y").equals(option)) {
                System.out.println("Bye bye :)");
                break;
            }
        }
        }

    public String userChoice(){
        while (true){
           String choices = readInput("Your choice (Rock/Paper/Scissors)?");
           if (rpsChoices.contains(choices)){
               return choices;
           }
           else {
               System.out.println("I do not understand " + choices + ". Could you try again?");
           }
        }
    }

    public String computerChoice(){
        String[] random = {"rock", "paper", "scissors"};
        Double randomNumber = Math.random() * 3;       //Double = float
        int intNumber = randomNumber.intValue();
        return random[intNumber];
    }

    public boolean findWinner(String winner, String looser) {
        if ((winner.equals("rock") && looser.equals("scissors")) ||
                (winner.equals("scissors") && looser.equals("paper")) ||
                (winner.equals("paper") && looser.equals("rock")))

        return true;
        else {
            return false;
        }
    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
